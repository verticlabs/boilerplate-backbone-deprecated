# Site name: Vertic Generator
## Abstract: Std. Vertic Generator for Yeoman, Bower and Grunt.

Fill out these things in the README:

* Meta data: Name of lead developer.
* Dependencies (like nodejs, ruby, grunt, bower and compass).
* Explanation about patterns or frameworks in use (like backbone or other internal framework, with link to further documentation).
* Relevant knowledge (did you hack an external library again?)

## Installation guide

Run `npm install` followed by `bower install`. Proceed to run application with `grunt serve`.

* Dependencies: nodejs, ruby, grunt, bower and compass

