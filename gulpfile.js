'use strict';

/**
*
* NAMING CONVENTIONS:
* -------------------
* Singleton-literals and prototype objects:  PascalCase
*
* Functions and public variables:            camelCase
*
* Global variables and constants:            UPPERCASE
*
* Private variables:                         _underscorePrefix
*
*/

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	browserSync = require('browser-sync'),
	browserify = require('browserify'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	transform = require('vinyl-transform'),
	stringify = require('stringify');

var watchify = require('watchify'),
	debowerify = require('debowerify');

var server = require('gulp-express');

var $ = require('gulp-load-plugins')(),
	_ = { app: 'app', dist: 'dist', reload: browserSync.reload };

// ✓ styles
gulp.task('styles', function () {
	return $.rubySass(_.app + '/styles/style.scss', {
		style: 'expanded',
		compass: true
	})
	.pipe(gulp.dest(_.dist + '/styles'))
	.pipe(_.reload({ stream: true }));
});

// ✓ browserify
var bundler = watchify(browserify('./' + _.app + '/scripts/main.js'), watchify.args);

gulp.task('browserify', function () {
	function bundle() {
		return bundler.bundle()
			// log errors if they happen
			.on('error', gutil.log.bind(gutil, 'Browserify Error'))
			.pipe(source('bundle.js'))
			// optional, remove if you dont want sourcemaps
			.pipe(buffer())
			.pipe($.sourcemaps.init({ loadMaps: true })) // loads map from browserify file
			.pipe($.sourcemaps.write('./')) // writes .map file
			//
			.pipe(gulp.dest(_.dist + '/scripts'));		
	}

	bundler
		// Testing browserify-shim, therefore this is uncommented
		// .transform(debowerify)
		.transform(stringify(['.html']))

		// Output build logs to terminal
		.on('log', gutil.log)
		.on('update', function () {
			bundle();
		});

	return bundle();
});

// ✓ jshint
gulp.task('jshint', function () {
	return gulp.src([
			_.app + '/scripts/**/*.js',
			'!' + _.app + '/scripts/vendor/**/*.js'
 		])
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish'))
		.pipe($.jshint.reporter('fail'));
});

// ✓ html
gulp.task('html', ['styles'], function () {
	var lazypipe = require('lazypipe');
	var cssChannel = lazypipe()
		.pipe($.csso)
		.pipe($.replace, 'fonts');

	var assets = $.useref.assets({ searchPath: '{.tmp, app}' });

	return gulp.src(_.app + '/*.html')
		.pipe(assets)
		.pipe($.if('*.js', $.uglify()))
		.pipe($.if('*.css', cssChannel()))
		.pipe(assets.restore())
		.pipe($.useref())
		.pipe($.if('*.html', $.minifyHtml({ conditionals: true, loose: true })))
		.pipe(gulp.dest(_.dist));
});

// ✓ images
gulp.task('images', function () {
	return gulp.src(_.app + '/assets/images/**/*')
		.pipe($.cache($.imagemin({
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest(_.dist + '/assets/images'));
});

// ✓ watch
gulp.task('watch', ['styles', 'browserify'], function () {
	browserSync({
		server: {
			// We listen to both app and dist (dist serves some static assets)
			baseDir: [_.app, _.dist]
		},
		// We're using Google Chrome as standard @ Vertic
		browser: 'google chrome'
	});

	gulp.watch(_.app + '/styles/**/*.scss', ['styles']);
	gulp.watch(['*.html'], { cwd: _.app }, _.reload);
	gulp.watch(['scripts/bundle.js'], { cwd: _.dist }, _.reload);
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('build', ['browserify', 'html', 'images'], function () {
	return gulp.src('dist/**/*')
		.pipe($.size({ title: 'build', gzip: true }))
		.once('end', function () {
			process.exit();
		});
});

gulp.task('default', ['watch']);
