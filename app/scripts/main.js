/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library');

// Document ready
$(function () {
	// Kick off

	console.log($, _, Core);
});
